// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:dio/dio.dart' as _i6;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:internet_connection_checker_plus/internet_connection_checker_plus.dart'
    as _i3;
import 'package:rick_and_morty/core/packages/network_info.dart' as _i4;
import 'package:rick_and_morty/feature/data/data_sources/local/person_local_data_source.dart'
    as _i7;
import 'package:rick_and_morty/feature/data/data_sources/remote/person_remote_data_source.dart'
    as _i8;
import 'package:rick_and_morty/feature/data/repositories/remote/remote_person_repository.dart'
    as _i10;
import 'package:rick_and_morty/feature/domain/repositories/person_repository.dart'
    as _i9;
import 'package:rick_and_morty/feature/domain/use_cases/get_all_person_usecase.dart'
    as _i12;
import 'package:rick_and_morty/feature/domain/use_cases/search_person_usecase.dart'
    as _i11;
import 'package:rick_and_morty/feature/presentation/pages/home_page/logic/home_cubit.dart'
    as _i13;
import 'package:rick_and_morty/service_locator/register_module.dart' as _i14;
import 'package:shared_preferences/shared_preferences.dart' as _i5;

extension GetItInjectableX on _i1.GetIt {
// initializes the registration of main-scope dependencies inside of GetIt
  Future<_i1.GetIt> init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) async {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    final registerModule = _$RegisterModule();
    gh.lazySingleton<_i3.InternetConnection>(
        () => registerModule.connectionCheckerPlus);
    gh.lazySingleton<_i4.NetworkInfo>(() =>
        _i4.NetworkInfoImpl(connectionChecker: gh<_i3.InternetConnection>()));
    await gh.factoryAsync<_i5.SharedPreferences>(
      () => registerModule.prefs,
      preResolve: true,
    );
    gh.factory<String>(
      () => registerModule.baseUrl,
      instanceName: 'baseUrl',
    );
    gh.lazySingleton<_i6.Dio>(
        () => registerModule.dio(gh<String>(instanceName: 'baseUrl')));
    gh.lazySingleton<_i7.PersonLocalDataSource>(
        () => _i7.PersonLocalDataSourceImp(prefs: gh<_i5.SharedPreferences>()));
    gh.lazySingleton<_i8.PersonRemoteDataSource>(
        () => _i8.PersonRemoteDataSourceImpl(dio: gh<_i6.Dio>()));
    gh.lazySingleton<_i9.PersonRepository>(() => _i10.RemotePersonRepository(
          remoteDataSource: gh<_i8.PersonRemoteDataSource>(),
          networkInfo: gh<_i4.NetworkInfo>(),
        ));
    gh.lazySingleton<_i11.SearchPersonUseCase>(() =>
        _i11.SearchPersonUseCase(personRepository: gh<_i9.PersonRepository>()));
    gh.lazySingleton<_i12.GetAllPersonUseCase>(() =>
        _i12.GetAllPersonUseCase(personRepository: gh<_i9.PersonRepository>()));
    gh.factory<_i13.HomeCubit>(() => _i13.HomeCubit(
          getAllPersonUseCase: gh<_i12.GetAllPersonUseCase>(),
          searchPersonUseCase: gh<_i11.SearchPersonUseCase>(),
        ));
    return this;
  }
}

class _$RegisterModule extends _i14.RegisterModule {}
