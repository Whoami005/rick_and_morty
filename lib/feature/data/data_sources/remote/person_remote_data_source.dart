import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:rick_and_morty/core/error/exception.dart';
import 'package:rick_and_morty/feature/domain/entities/character_information_entity.dart';

abstract interface class PersonRemoteDataSource {
  /// Throws a [ServerException] for all error codes.

  /// https://rickandmortyapi.com/api/character/?page=1 endpoint.
  Future<CharacterInformationEntity> getAllPerson({required int page});

  /// https://rickandmortyapi.com/api/character/?name=morty endpoint.
  Future<CharacterInformationEntity> searchPerson({required String name});
}

@LazySingleton(as: PersonRemoteDataSource)
class PersonRemoteDataSourceImpl implements PersonRemoteDataSource {
  final Dio dio;

  const PersonRemoteDataSourceImpl({required this.dio});

  Future<CharacterInformationEntity> _getPersonFromQuery({
    required String query,
  }) async {
    final url = 'character/?$query';

    final response = await dio.get(url);

    if (response.statusCode == 200) {
      return CharacterInformationEntity.fromJson(response.data);
    } else {
      throw ServerException();
    }
  }

  @override
  Future<CharacterInformationEntity> getAllPerson({required int page}) =>
      _getPersonFromQuery(query: 'page=$page');

  @override
  Future<CharacterInformationEntity> searchPerson({required String name}) =>
      _getPersonFromQuery(query: 'name=$name');
}
