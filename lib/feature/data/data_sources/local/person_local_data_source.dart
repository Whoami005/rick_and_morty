import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:rick_and_morty/common/keys/shared_preferences_keys.dart';
import 'package:rick_and_morty/core/error/exception.dart';
import 'package:rick_and_morty/feature/domain/entities/character_information_entity.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract interface class PersonLocalDataSource {
  /// Throws a [CacheException] for all error codes.

  /// Получение последних кэшированных данных.
  Future<CharacterInformationEntity> getLastPersonsFromCache();

  /// Сохранение данных в кэш.
  Future<void> personToCache({required CharacterInformationEntity persons});
}

@LazySingleton(as: PersonLocalDataSource)
class PersonLocalDataSourceImp implements PersonLocalDataSource {
  final SharedPreferences prefs;

  const PersonLocalDataSourceImp({required this.prefs});

  @override
  Future<CharacterInformationEntity> getLastPersonsFromCache() async {
    final cache = prefs.getString(SharedPreferencesKeys.cachedCharacters);

    if (cache != null) {
      final lastPersons =
          CharacterInformationEntity.fromJson(jsonDecode(cache));

      return Future.value(lastPersons);
    } else {
      throw CacheException();
    }
  }

  @override
  Future<void> personToCache({
    required CharacterInformationEntity persons,
  }) async {
    final characters = jsonEncode(persons.toJson());

    debugPrint('CACHED: ${persons.results.length}');

    await prefs.setString(SharedPreferencesKeys.cachedCharacters, characters);
  }
}
