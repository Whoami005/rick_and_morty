import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:rick_and_morty/core/error/exception.dart';
import 'package:rick_and_morty/core/error/failure.dart';
import 'package:rick_and_morty/core/packages/network_info.dart';
import 'package:rick_and_morty/feature/data/data_sources/remote/person_remote_data_source.dart';
import 'package:rick_and_morty/feature/domain/entities/character_information_entity.dart';
import 'package:rick_and_morty/feature/domain/repositories/person_repository.dart';

@LazySingleton(as: PersonRepository)
class RemotePersonRepository implements PersonRepository {
  final PersonRemoteDataSource remoteDataSource;
  final NetworkInfo networkInfo;

  const RemotePersonRepository({
    required this.remoteDataSource,
    required this.networkInfo,
  });

  Future<Either<Failure, CharacterInformationEntity>> _getPersons(
    Future<CharacterInformationEntity> Function() getPersons,
  ) async {
    try {
      final isConnected = await networkInfo.isConnected;

      if (isConnected) {
        final response = await getPersons();

        return Right(response);
      } else {
        return Left(ConnectionFailure());
      }
    } on ServerException {
      return Left(ServerFailure());
    } catch (e) {
      return Left(UnknownFailure());
    }
  }

  @override
  Future<Either<Failure, CharacterInformationEntity>> getAllPersons({
    required int page,
  }) async =>
      await _getPersons(() => remoteDataSource.getAllPerson(page: page));

  @override
  Future<Either<Failure, CharacterInformationEntity>> searchPerson({
    required String name,
  }) async =>
      await _getPersons(() => remoteDataSource.searchPerson(name: name));
}
