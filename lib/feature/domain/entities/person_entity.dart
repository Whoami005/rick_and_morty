import 'package:equatable/equatable.dart';
import 'package:rick_and_morty/core/enums/character_gender.dart';
import 'package:rick_and_morty/core/enums/character_status.dart';
import 'package:rick_and_morty/feature/domain/entities/location_entity.dart';

class PersonEntity extends Equatable {
  final int id;
  final String name;
  final CharacterStatus status;
  final String species;
  final String type;
  final CharacterGender gender;
  final LocationEntity? origin;
  final LocationEntity? location;
  final String image;
  final List<String> episode;
  final String url;
  final DateTime created;

  const PersonEntity({
    required this.id,
    required this.name,
    required this.status,
    required this.species,
    required this.type,
    required this.gender,
    required this.origin,
    required this.location,
    required this.image,
    required this.episode,
    required this.url,
    required this.created,
  });

  @override
  List<Object?> get props => [
        id,
        name,
        status,
        species,
        type,
        gender,
        origin,
        location,
        image,
        episode,
        url,
        created,
      ];

  factory PersonEntity.fromJson(Map<String, dynamic> json) {
    return PersonEntity(
      id: json["id"],
      name: json["name"],
      status: CharacterStatus.fromJson(json["status"]),
      species: json["species"],
      type: json["type"],
      gender: CharacterGender.fromJson(json["gender"]),
      origin: json["origin"] != null
          ? LocationEntity.fromJson(json["origin"])
          : null,
      location: json["origin"] != null
          ? LocationEntity.fromJson(json["location"])
          : null,
      image: json["image"],
      episode: json["episode"] != null ? json["episode"].cast<String>() : [],
      url: json["url"],
      created: DateTime.parse(json["created"]),
    );
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "status": status.toJson(),
        "species": species,
        "type": type,
        "gender": gender.toJson(),
        "origin": origin?.toJson(),
        "location": location?.toJson(),
        "image": image,
        "episode": episode,
        "url": url,
        "created": created.toIso8601String(),
      };

  PersonEntity copyWith({
    int? id,
    String? name,
    CharacterStatus? status,
    String? species,
    String? type,
    CharacterGender? gender,
    LocationEntity? origin,
    LocationEntity? location,
    String? image,
    List<String>? episode,
    String? url,
    DateTime? created,
  }) {
    return PersonEntity(
      id: id ?? this.id,
      name: name ?? this.name,
      status: status ?? this.status,
      species: species ?? this.species,
      type: type ?? this.type,
      gender: gender ?? this.gender,
      origin: origin ?? this.origin,
      location: location ?? this.location,
      image: image ?? this.image,
      episode: episode ?? this.episode,
      url: url ?? this.url,
      created: created ?? this.created,
    );
  }
}
