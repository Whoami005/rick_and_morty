import 'package:equatable/equatable.dart';
import 'package:rick_and_morty/feature/domain/entities/page_information_entity.dart';
import 'package:rick_and_morty/feature/domain/entities/person_entity.dart';

class CharacterInformationEntity extends Equatable {
  final PageInformationEntity info;
  final List<PersonEntity> results;

  const CharacterInformationEntity({
    this.info = const PageInformationEntity(),
    this.results = const [],
  });

  @override
  List<Object?> get props => [info, results];

  factory CharacterInformationEntity.fromJson(Map<String, dynamic> json) {
    final List<PersonEntity> personages = [];

    if (json["results"] != null) {
      for (final person in json["results"]) {
        personages.add(PersonEntity.fromJson(person));
      }
    }

    return CharacterInformationEntity(
      info: PageInformationEntity.fromJson(json["info"] ?? {}),
      results: personages,
    );
  }

  Map<String, dynamic> toJson() => {
        "info": info.toJson(),
        "results": [for (final person in results) person.toJson()],
      };

  CharacterInformationEntity copyWith({
    PageInformationEntity? info,
    List<PersonEntity>? results,
  }) {
    return CharacterInformationEntity(
      info: info ?? this.info,
      results: results ?? this.results,
    );
  }
}
