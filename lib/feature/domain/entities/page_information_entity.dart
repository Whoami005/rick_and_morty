import 'package:equatable/equatable.dart';

class PageInformationEntity extends Equatable {
  final int count;
  final int pages;
  final String? next;
  final String? prev;

  const PageInformationEntity({
    this.count = 0,
    this.pages = 0,
    this.next,
    this.prev,
  });

  @override
  List<Object?> get props => [count, pages, next, prev];

  factory PageInformationEntity.fromJson(Map<String, dynamic> json) {
    return PageInformationEntity(
      count: json["count"] ?? 0,
      pages: json["pages"] ?? 0,
      next: json["next"],
      prev: json["prev"],
    );
  }

  Map<String, dynamic> toJson() =>
      {"count": count, "pages": pages, "next": next, "prev": prev};

  PageInformationEntity copyWith({
    int? count,
    int? pages,
    String? next,
    String? prev,
  }) {
    return PageInformationEntity(
      count: count ?? this.count,
      pages: pages ?? this.pages,
      next: next ?? this.next,
      prev: prev ?? this.prev,
    );
  }
}
