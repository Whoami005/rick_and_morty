import 'package:equatable/equatable.dart';

class LocationEntity extends Equatable {
  final String name;
  final String url;

  const LocationEntity({required this.name, required this.url});

  @override
  List<Object?> get props => [name, url];

  factory LocationEntity.fromJson(Map<String, dynamic> json) {
    return LocationEntity(name: json["name"], url: json["url"]);
  }

  Map<String, dynamic> toJson() => {"name": name, "url": url};

  LocationEntity copyWith({String? name, String? url}) {
    return LocationEntity(name: name ?? this.name, url: url ?? this.url);
  }
}
