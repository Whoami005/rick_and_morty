import 'package:dartz/dartz.dart';
import 'package:rick_and_morty/core/error/failure.dart';
import 'package:rick_and_morty/feature/domain/entities/character_information_entity.dart';

abstract interface class PersonRepository {
  Future<Either<Failure, CharacterInformationEntity>> getAllPersons({
    required int page,
  });

  Future<Either<Failure, CharacterInformationEntity>> searchPerson({
    required String name,
  });
}
