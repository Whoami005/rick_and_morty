import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';
import 'package:rick_and_morty/core/error/failure.dart';
import 'package:rick_and_morty/core/use_cases/use_case.dart';
import 'package:rick_and_morty/feature/domain/entities/character_information_entity.dart';
import 'package:rick_and_morty/feature/domain/repositories/person_repository.dart';

@lazySingleton
class GetAllPersonUseCase
    implements UseCase<CharacterInformationEntity, PagePersonParams> {
  final PersonRepository _personRepository;

  const GetAllPersonUseCase({required PersonRepository personRepository})
      : _personRepository = personRepository;

  @override
  Future<Either<Failure, CharacterInformationEntity>> call(
      PagePersonParams params) async {
    return await _personRepository.getAllPersons(page: params.page);
  }
}

class PagePersonParams extends Equatable {
  final int page;

  const PagePersonParams({required this.page});

  @override
  List<Object?> get props => [page];
}
