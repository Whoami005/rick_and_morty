import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';
import 'package:rick_and_morty/core/error/failure.dart';
import 'package:rick_and_morty/core/use_cases/use_case.dart';
import 'package:rick_and_morty/feature/domain/entities/character_information_entity.dart';
import 'package:rick_and_morty/feature/domain/repositories/person_repository.dart';

@lazySingleton
class SearchPersonUseCase
    implements UseCase<CharacterInformationEntity, SearchPersonParams> {
  final PersonRepository _personRepository;

  const SearchPersonUseCase({required PersonRepository personRepository})
      : _personRepository = personRepository;

  @override
  Future<Either<Failure, CharacterInformationEntity>> call(
      SearchPersonParams params) async {
    return await _personRepository.searchPerson(name: params.name);
  }
}

class SearchPersonParams extends Equatable {
  final String name;

  const SearchPersonParams({required this.name});

  @override
  List<Object?> get props => [name];
}
