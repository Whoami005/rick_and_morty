import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CachedNetworkImageWidget extends StatelessWidget {
  final String imageUrl;
  final double width;
  final Widget? placeholder;
  final Widget? errorImage;

  const CachedNetworkImageWidget({
    super.key,
    required this.imageUrl,
    this.width = 166,
    this.placeholder,
    this.errorImage,
  });

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: imageUrl,
      width: width,
      fit: BoxFit.cover,
      placeholder: (context, url) {
        return placeholder ?? const Center(child: CircularProgressIndicator());
      },
      errorWidget: (context, url, error) {
        return errorImage ?? Image.asset('assets/images/no_image.jpg');
      },
    );
  }
}
