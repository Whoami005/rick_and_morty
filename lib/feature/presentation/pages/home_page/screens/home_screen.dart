import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_and_morty/common/theme/app_colors.dart';
import 'package:rick_and_morty/common/theme/app_icons.dart';
import 'package:rick_and_morty/core/enums/logic_state_status.dart';
import 'package:rick_and_morty/feature/presentation/pages/home_page/logic/home_cubit.dart';
import 'package:rick_and_morty/feature/presentation/pages/home_page/widgets/person_card.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late final ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    _scrollController.addListener(() async => await pagination());
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  Future<void> pagination() async {
    final cubit = context.read<HomeCubit>();
    final position = _scrollController.position;
    final isEndScroll = position.atEdge && position.pixels != 0;
    final nextPageIsNotNull = cubit.state.persons.info.next != null;

    if (isEndScroll && nextPageIsNotNull) await cubit.updateCharacters();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 2,
        title: const Text('Персонажи'),
        centerTitle: true,
      ),
      body: SafeArea(
        child: BlocConsumer<HomeCubit, HomeState>(
          listener: (BuildContext context, HomeState state) async {
            if (state.connectionError) {
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(
                  showCloseIcon: true,
                  behavior: SnackBarBehavior.floating,
                  content: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Icon(AppIcons.warning, size: 14, color: AppColors.red),
                      SizedBox(width: 8),
                      Text('Нет подключения к сети'),
                    ],
                  ),
                ),
              );
            }
          },
          builder: (context, state) {
            final cubit = context.read<HomeCubit>();

            return RefreshIndicator(
              onRefresh: () async =>
                  await cubit.updateCharacters(onRefresh: true),
              child: switch (state.status) {
                LogicStateStatus.success when state.persons.results.isEmpty =>
                  const Center(child: Text('Данные пусты')),
                LogicStateStatus.success => ListView.separated(
                    controller: _scrollController,
                    padding:
                        const EdgeInsets.symmetric(vertical: 20, horizontal: 5),
                    itemCount: state.persons.results.length,
                    itemBuilder: (context, index) {
                      final person = state.persons.results[index];

                      return PersonCard(person: person);
                    },
                    separatorBuilder: (BuildContext context, int index) =>
                        const SizedBox(height: 10),
                  ),
                LogicStateStatus.error =>
                  const Center(child: Text('Что-то пошло не так')),
                _ => const Center(child: CircularProgressIndicator()),
              },
            );
          },
        ),
      ),
    );
  }
}
