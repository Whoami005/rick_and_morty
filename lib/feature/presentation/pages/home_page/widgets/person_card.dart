import 'package:flutter/material.dart';
import 'package:rick_and_morty/common/theme/app_colors.dart';
import 'package:rick_and_morty/common/theme/app_text_style.dart';
import 'package:rick_and_morty/core/enums/character_status.dart';
import 'package:rick_and_morty/feature/domain/entities/person_entity.dart';
import 'package:rick_and_morty/feature/presentation/widgets/cached_network_image_widget.dart';

class PersonCard extends StatelessWidget {
  final PersonEntity person;

  const PersonCard({super.key, required this.person});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: IntrinsicHeight(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: CachedNetworkImageWidget(imageUrl: person.image),
            ),
            const SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    person.name,
                    style: AppTextStyle.title20bold(),
                    maxLines: 1,
                  ),
                  const SizedBox(height: 5),
                  Row(
                    children: [
                      Icon(
                        Icons.circle,
                        color: switch (person.status) {
                          CharacterStatus.Alive => AppColors.green,
                          CharacterStatus.Dead => AppColors.red,
                          CharacterStatus.Unknown => AppColors.grey,
                        },
                        size: 16,
                      ),
                      const SizedBox(width: 5),
                      Text(person.status.name),
                    ],
                  ),
                  const SizedBox(height: 10),
                  Text(
                    'Gender: ',
                    style: AppTextStyle.bodyText(color: AppColors.grey),
                  ),
                  Text(person.gender.name),
                  if (person.location != null) ...[
                    const SizedBox(height: 10),
                    Text(
                      'Last known location: ',
                      style: AppTextStyle.bodyText(color: AppColors.grey),
                    ),
                    Text(person.location!.name),
                  ],
                  if (person.origin != null) ...[
                    const SizedBox(height: 10),
                    Text(
                      'First seen in: ',
                      style: AppTextStyle.bodyText(color: AppColors.grey),
                    ),
                    Text(
                      person.origin!.name,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                  const SizedBox(height: 5),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
