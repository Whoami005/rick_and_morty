part of 'home_cubit.dart';

class HomeState extends Equatable {
  final LogicStateStatus status;
  final CharacterInformationEntity persons;
  final int currentPage;
  final bool connectionError;

  const HomeState({
    this.status = LogicStateStatus.initial,
    this.persons = const CharacterInformationEntity(),
    this.currentPage = 1,
    this.connectionError = false,
  });

  @override
  List<Object?> get props => [status, persons, currentPage, connectionError];

  Map<String, dynamic> toJson() {
    return {
      'status': status.toJson(),
      'persons': persons.toJson(),
      'current_page': currentPage,
      // 'connection_error': connectionError,
    };
  }

  factory HomeState.fromJson(Map<String, dynamic> json) {
    return HomeState(
      status: LogicStateStatus.fromJson(json['status']),
      persons: CharacterInformationEntity.fromJson(json['persons']),
      currentPage: json['current_page'],
      // connectionError: json['connection_error'],
    );
  }

  HomeState copyWith({
    LogicStateStatus? status,
    CharacterInformationEntity? persons,
    int? currentPage,
    bool? connectionError,
  }) {
    return HomeState(
      status: status ?? this.status,
      persons: persons ?? this.persons,
      currentPage: currentPage ?? this.currentPage,
      connectionError: connectionError ?? false,
    );
  }
}
