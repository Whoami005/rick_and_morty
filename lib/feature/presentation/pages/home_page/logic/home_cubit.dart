import 'package:equatable/equatable.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:rick_and_morty/core/enums/logic_state_status.dart';
import 'package:rick_and_morty/core/error/failure.dart';
import 'package:rick_and_morty/feature/domain/entities/character_information_entity.dart';
import 'package:rick_and_morty/feature/domain/use_cases/get_all_person_usecase.dart';
import 'package:rick_and_morty/feature/domain/use_cases/search_person_usecase.dart';

part 'home_state.dart';

@injectable
class HomeCubit extends HydratedCubit<HomeState> {
  final GetAllPersonUseCase _getAllPersonUseCase;
  final SearchPersonUseCase _searchPersonUseCase;

  HomeCubit({
    required GetAllPersonUseCase getAllPersonUseCase,
    required SearchPersonUseCase searchPersonUseCase,
  })  : _getAllPersonUseCase = getAllPersonUseCase,
        _searchPersonUseCase = searchPersonUseCase,
        super(const HomeState());

  Future<void> cacheInitialization() async {
    final storageIsEmpty = HydratedBloc.storage.read(storageToken) == null;
    final correctState =
        state.status.isSuccess && state.persons.results.isNotEmpty;

    if (storageIsEmpty || !correctState) await updateCharacters();
  }

  Future<void> updateCharacters({bool onRefresh = false}) async {
    final page = onRefresh ? 1 : state.currentPage;
    final params = PagePersonParams(page: page);

    final response = await _getAllPersonUseCase(params);

    response.fold(
      (error) => switch (error) {
        ConnectionFailure() => emit(state.copyWith(
            status: LogicStateStatus.success,
            connectionError: true,
          )),
        _ => emit(state.copyWith(status: LogicStateStatus.error)),
      },
      (persons) => emit(state.copyWith(
        status: LogicStateStatus.success,
        persons: state.persons.copyWith(
          info: persons.info,
          results: !onRefresh
              ? [...state.persons.results, ...persons.results]
              : persons.results,
        ),
        currentPage: page + 1,
      )),
    );
  }

  Future<void> searchPerson({required String name}) async {
    emit(state.copyWith(status: LogicStateStatus.loading));

    final response = await _searchPersonUseCase(SearchPersonParams(name: name));

    response.fold(
      (error) => emit(state.copyWith(status: LogicStateStatus.error)),
      (persons) => emit(state.copyWith(
        status: LogicStateStatus.success,
        persons: persons,
      )),
    );
  }

  @override
  HomeState fromJson(Map<String, dynamic> json) => HomeState.fromJson(json);

  @override
  Map<String, dynamic> toJson(HomeState state) => state.toJson();
}
