import 'package:flutter/material.dart';

class AppTextStyle {
  AppTextStyle._();

  static TextStyle title20bold() {
    return const TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.bold,
    );
  }

  static TextStyle bodyText({Color? color}) {
    return TextStyle(
      fontSize: 14,
      color: color,
    );
  }
}
