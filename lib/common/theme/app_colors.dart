import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  static const Color black = Colors.black;
  static const Color green = Colors.green;
  static const Color red = Colors.red;
  static const Color grey = Colors.grey;
  static const Color hintColor = Color(0xFF717578);
}