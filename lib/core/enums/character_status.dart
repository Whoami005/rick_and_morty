// ignore_for_file: constant_identifier_names

enum CharacterStatus {
  Alive,
  Dead,
  Unknown;

  String toJson() => name;

  static CharacterStatus fromJson(String name) {
    try {
      return values.byName(name);
    } catch (e) {
      return Unknown;
    }
  }

  bool get isAlive => index == Alive.index;

  bool get isDead => index == Dead.index;

  bool get isUnknown => index == Unknown.index;
}
