// ignore_for_file: constant_identifier_names

enum CharacterGender {
  Female,
  Male,
  Genderless,
  Unknown;

  String toJson() => name;

  static CharacterGender fromJson(String name) {
    try {
      return values.byName(name);
    } catch (e) {
      return Unknown;
    }
  }

  bool get isFemale => index == Female.index;

  bool get isMale => index == Male.index;

  bool get isGenderless => index == Genderless.index;

  bool get isUnknown => index == Unknown.index;
}
